# Calculator App

This starter code is written from scratch in Kotlin,
based off of Andrew and Patricia's code (see the [original repo](https://gitlab.com/abodza/calculator-starter))

## How to use

1.  Fork this repository (see this tutorial on [project forking workflow](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)).

2.  From your forked version of this repository, clone the project on your computer.

3.  Open the project in the `calculatorapp/` directory, with Android Studio, **do not open the entire repo with Android Studio, just the project**.

4.  Build and run the project, if you see the following screen, you can start the lab :

    ![Initial application screenshot](initial_app_screenshot.png "Initial application screenshot")

## TODOs

**Push the changes to your GitLab fork as you progress.**

- [ ] Move hardcoded strings to the appropriate XML file.

- [ ] Add an EditText for a second number.

- [ ] Add more buttons for subtraction, multiplication and division.

- [ ] Add validation for empty input, invalid number or divide by zero, each displaying an error message.

- [ ] Add a button to clear the number fields and TextViews.

- [ ] Change the color of a few TextViews or buttons

- [ ] (Bonus) Make the app your own by giving it an icon ([nice icon generator](https://romannurik.github.io/AndroidAssetStudio/icons-launcher.html))

## Example of the finished result

You can try and replicate this layout, or do your own thing,

![End Result](end_result.jpg "End Result")
