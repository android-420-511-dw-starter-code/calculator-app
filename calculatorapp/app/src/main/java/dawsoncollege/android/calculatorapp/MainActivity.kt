package dawsoncollege.android.calculatorapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dawsoncollege.android.calculatorapp.databinding.ActivityMainBinding

// TODO : add an EditText for the second number

class MainActivity : AppCompatActivity() {
    
    // TODO : read on https://kotlinlang.org/docs/properties.html#late-initialized-properties-and-variables
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // TODO : change the addButton onClickListener so it adds
        binding.addButton.setOnClickListener {
            binding.resultTextView.text = binding.firstNumberEditText.text.toString()
        }

        // TODO : add more onClickListener for subtraction, multiplication and division

        // TODO : add an onClickListener for a clear action
    }

    private fun validateNumberStr(numberStr: String): String? {
        /* This method is optional, it's a suggestion if you wish to
         * have your validation in one place, otherwise you can delete it
         */

        // TODO : validate numberStr, return a String error message if need be

        // Here, null means there is no error
        return null
    }
}